#
# Executes commands at the start of an interactive session.
#

# add homebrewed binaries at beginning of path
export PATH=/usr/local/bin:/usr/local/sbin:$PATH

# add homebrew completion's
# https://docs.brew.sh/Shell-Completion#configuring-completions-in-zsh
if type brew &>/dev/null; then
  FPATH=$(brew --prefix)/share/zsh/site-functions:$FPATH
  autoload -Uz compinit && compinit
fi

# Add some aliases
alias ls='exa'
alias ll='exa -alh'

# Add fuzzy search
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_DEFAULT_COMMAND='fd -HI --follow --exclude ".git" --type f'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

# use jump
eval "$(jump shell)"

# use Starship prompt
eval "$(starship init zsh)"
