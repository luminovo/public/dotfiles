# Dotfiles

Shared dotfiles used for Gitpod

### Dotbot
We manage our dotfiles with [Dotbot](https://github.com/anishathalye/dotbot).
To link all dotfiles to where they belong run `./install`.
